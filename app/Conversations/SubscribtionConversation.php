<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Log;

class SubscribtionConversation extends Conversation
{
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $question = Question::create('Please select a package to subscribe')
            ->addButtons(
                [
                    Button::create('1 Month @ 2000')->value('1 Month'),
                    Button::create('1 Week @ 1000')->value('1 Week'),
                    Button::create('1 Day @ 200')->value('1 Day'),
                ]
            );
        $this->ask($question, function (Answer $answer) {
            if ($answer->getText() === '1 Month') {
                $this->mpesa('1 Month');
            }
            if ($answer->getText() === '1 Week') {
                $this->mpesa('1 Month');
            }
            if ($answer->getText() === '1 Day') {
                $this->mpesa('1 Month');
            }
        });
    }

    public function mpesa($month)
    {
        $this->bot->ask('Share you contact so that we can use the number to send subscription info', function (Answer $answer) use ($month) {
            $phonenumber = $this->bot->userStorage()->get('phonenumber');
            if (!isset($phonenumber)) {
                $contact = collect($answer->getMessage()->getPayload())->toArray();

                if (!isset($contact['contact'])) {
                    $this->bot->reply(" We need your safaricom number. ");
                }
                $this->bot->userStorage()->save([
                    'phonenumber' => $contact['contact']['phone_number'],
                    'name'        => $contact['contact']['first_name'],
                    'user_id'     => $contact['contact']['user_id'],
                ]);
                $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                $token = accesstoken();
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type:application/json',
                    "Authorization:Bearer {$token}"
                )); //setting custom header
                $password = base64_encode('684796' . '056f96737d968ba4f5d858b13b764930c4a4f952e89cc170e7968df207c454ca' . $date = date('YmdHis'));

                $curl_post_data = array(
                    //Fill in the request parameters with valid values
                    'BusinessShortCode' => '684796',
                    'Password'          => $password,
                    'Timestamp'         => $date,
                    'TransactionType'   => 'CustomerPayBillOnline',
                    'Amount'            => '10',
                    'PartyA'            => "{$phonenumber}",
                    'PartyB'            => '684796',
                    'PhoneNumber'       => "{$phonenumber}",
                    'CallBackURL'       => 'https://7ea70576.ngrok.io/safaricom',
                    'AccountReference'  => 'Game',
                    'TransactionDesc'   => "{$month} for {$phonenumber}"
                );

                $data_string = json_encode($curl_post_data);

                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

                $curl_response = curl_exec($curl);

                if ($curl_response->ResponseCode === '0' || $curl_response->ResponseCode === 0) {
                    $this->bot->typesAndWaits(2000);
                    $this->bot->reply("We are confirming your transaction. You will get a confirmation text of your subscription shortly after we confirm your subscription");
                } else {
                    $this->bot->reply('There has been a problem with the transaction');
                }


            } else {
                $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                $token = accesstoken();
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type:application/json',
                    "Authorization:Bearer {$token}"
                )); //setting custom header
                $password = base64_encode('684796' . '056f96737d968ba4f5d858b13b764930c4a4f952e89cc170e7968df207c454ca' . $date = date('YmdHis'));

                $curl_post_data = array(
                    //Fill in the request parameters with valid values
                    'BusinessShortCode' => '684796',
                    'Password'          => $password,
                    'Timestamp'         => $date,
                    'TransactionType'   => 'CustomerPayBillOnline',
                    'Amount'            => '10',
                    'PartyA'            => "{$phonenumber}",
                    'PartyB'            => '684796',
                    'PhoneNumber'       => "{$phonenumber}",
                    'CallBackURL'       => 'https://7ea70576.ngrok.io/safaricom',
                    'AccountReference'  => 'Game',
                    'TransactionDesc'   => "{$month} for {$phonenumber}"
                );

                $data_string = json_encode($curl_post_data);

                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

                $curl_response = curl_exec($curl);

                if ($curl_response->ResponseCode === '0' || $curl_response->ResponseCode === 0) {
                    $this->bot->typesAndWaits(2000);
                    $this->bot->reply("We are confirming your transaction. You will get a confirmation text of your subscription shortly after we confirm your subscription");
                } else {
                    $this->bot->reply('There has been a problem with the transaction');
                }
            }

        }, ['reply_markup' => json_encode([
            'keyboard'          => [
                [
                    ['text' => 'Share your safaricom number', 'request_contact' => true]
                ]
            ],
            'one_time_keyboard' => true,
            'resize_keyboard'   => true
        ])]);


    }

    public function initiatePayment($number, $months)
    {
        $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $token = accesstoken();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json',
            "Authorization:Bearer {$token}"
        )); //setting custom header
        $password = base64_encode('684796' . '056f96737d968ba4f5d858b13b764930c4a4f952e89cc170e7968df207c454ca' . $date = date('YmdHis'));

        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'BusinessShortCode' => '684796',
            'Password'          => $password,
            'Timestamp'         => $date,
            'TransactionType'   => 'CustomerPayBillOnline',
            'Amount'            => '10',
            'PartyA'            => "{$number}",
            'PartyB'            => '684796',
            'PhoneNumber'       => "{$number}",
            'CallBackURL'       => 'https://7ea70576.ngrok.io/safaricom',
            'AccountReference'  => 'Game',
            'TransactionDesc'   => "{$months} for {$number}"
        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);

        $this->bot->typesAndWaits(2000);
        $this->say("We are confirming your transaction. You will get a confirmation text of your subscription shortly after we confirm your subscription");
    }
}
