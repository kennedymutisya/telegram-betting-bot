<?php

namespace App\Conversations;

use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use Illuminate\Support\Facades\Log;

class ExampleConversation extends Conversation
{
    /**
     * First question
     */
    public function askReason()
    {
        $keyboard = [
            ['SportPesa', 'Betika', 'Tips'],
            ['Subscribe', 'My Subscription'],
        ];

        $this->ask('Thank you for visiting, please select a bookie',
            function (Answer $answer) {
                if ($answer->getText() === 'SportPesa') {
                    $this->sportPesa();
                }
                if ($answer->getText() === 'Betika') {
                    $this->betika();
                }
                if ($answer->getText() === 'Subscribe') {
                    $this->bot->startConversation(new SubscribtionConversation());
                }
                if ($answer->getText() === 'My Subscription') {

                    $this->say('Please subscribe first');
                }
                if ($answer->getText() === 'Tips') {
                    $this->say("Our tips will cost you KES 100 per tip. Click /start and select a bookie and the game you want. Pay on the pop up and get your booking");
                }
            }, ['reply_markup' => json_encode([
                'keyboard'          => $keyboard,
                'one_time_keyboard' => true,
                'resize_keyboard'   => true
            ])]
        );
    }

    public function betika()
    {
        list($d, $g) = $this->betikagames();

        $this->bot->ask("Select game", function (Answer $answer) use ($d) {
            $user = $this->bot->getUser()->getId();
            preg_match('/(?<=\| )\d+/', $answer->getMessage()->getPayload()['text'], $matches, PREG_OFFSET_CAPTURE);

            $odds = collect($d)->where('game_id', $matches[0][0])->first();
            $question = Question::create("Current Betika Odds are \n Home (1) : *{$odds['home_odd']}*\n Draw   (X) : *{$odds['neutral_odd']}* \n Away   (2) : *{$odds['away_odd']}*
         
            ")
                ->fallback('Unable to ask question')
                ->callbackId('ask_reason')
                ->addButtons([
                    Button::create('Buy Sure Tip')->value('joke')->additionalParameters([
                        'game_id' => $odds['game_id']
                    ]),
                    Button::create('BET')->value('quote'),
                ]);
            $this->bot->ask($question, function (Answer $answer) {
                if ($answer->isInteractiveMessageReply()) {
                    if ($answer->getValue() === 'joke') {

                        $this->ask("{$this->bot->getUser()->getFirstName()}, Whats your safaricom phone number, start with 2547", function (Answer $answer) {
                            $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

                            $curl = curl_init();
                            curl_setopt($curl, CURLOPT_URL, $url);
                            $token = accesstoken();
                            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                                'Content-Type:application/json',
                                "Authorization:Bearer {$token}"
                            )); //setting custom header
                            $password = base64_encode('684796' . '056f96737d968ba4f5d858b13b764930c4a4f952e89cc170e7968df207c454ca' . $date = date('YmdHis'));

                            $curl_post_data = array(
                                //Fill in the request parameters with valid values
                                'BusinessShortCode' => '684796',
                                'Password'          => $password,
                                'Timestamp'         => $date,
                                'TransactionType'   => 'CustomerPayBillOnline',
                                'Amount'            => '10',
                                'PartyA'            => "{$answer->getText()}",
                                'PartyB'            => '684796',
                                'PhoneNumber'       => "{$answer->getText()}",
                                'CallBackURL'       => 'https://12b529cb.ngrok.io/safaricom',
                                'AccountReference'  => 'Game',
                                'TransactionDesc'   => 'Game for'
                            );

                            $data_string = json_encode($curl_post_data);

                            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($curl, CURLOPT_POST, true);
                            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

                            $curl_response = curl_exec($curl);

                            $this->bot->typesAndWaits(4000);
                            $this->say("We are confirming your transaction. You will get the Game via text");

                        });

                    } else {
                        $this->say(Inspiring::quote());
                    }
                }
            }, [
                'parse_mode' => 'Markdown',
            ]);

        }, ['reply_markup' => json_encode([
            'keyboard'          => $g,
            'one_time_keyboard' => true,
            'resize_keyboard'   => true
        ])]);

    }

    public function askBookie()
    {
        $this->say('Which Bookie');
    }

    public function sportPesa()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->get('https://www.sportpesa.co.ke/api/todays/1/games?type=prematch&section=today&o=startTime&pag_count=30&pag_min=1');
        $d = collect(json_decode($res->getBody(), true));

        $g = collect($d)->map(function ($foota) {
//            Button::create("{$foota['competitors'][0]['name']} vs {$foota['competitors'][1]['name']} | {$foota['smsId']}")->value('joke');
            return ["{$foota['competitors'][0]['name']} vs {$foota['competitors'][1]['name']} | {$foota['smsId']}",];
        });
        $this->bot->ask("Select game", function (Answer $answer) use ($d) {
            preg_match('/(?<=\| )\d+/', $answer->getMessage()->getPayload()['text'], $matches, PREG_OFFSET_CAPTURE);

            $odds = collect($d)->where('smsId', $matches[0][0])->first();
//
            $question = Question::create("Current SportPesa Odds are \n Home (1) : *{$odds['markets'][0]['selections'][0]['odds']}*\n Draw   (X) : *{$odds['markets'][0]['selections'][1]['odds']}* \n Away   (2) : *{$odds['markets'][0]['selections'][2]['odds']}*
           
            ")
                ->fallback('Unable to ask question')
                ->callbackId('ask_reason')
                ->addButtons([
                    Button::create('Buy Sure Tip')->value('joke')->additionalParameters([
                        'game_id' => $odds['smsId']
                    ]),
                    Button::create('BET')->value('quote'),
                ]);
            $this->bot->ask($question, function (Answer $answer) {
                if ($answer->isInteractiveMessageReply()) {
                    if ($answer->getValue() === 'joke') {

                        $this->ask("{$this->bot->getUser()->getFirstName()}, Whats your safaricom phone number, start with 2547", function (Answer $answer) {
                            $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

                            $curl = curl_init();
                            curl_setopt($curl, CURLOPT_URL, $url);
                            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                                'Content-Type:application/json',
                                'Authorization:Bearer wm0gskEEza6NSqa01dE63TgNr4tZ'
                            )); //setting custom header
                            $password = base64_encode('684796' . '056f96737d968ba4f5d858b13b764930c4a4f952e89cc170e7968df207c454ca' . $date = date('YmdHis'));

                            $curl_post_data = array(
                                //Fill in the request parameters with valid values
                                'BusinessShortCode' => '684796',
                                'Password'          => $password,
                                'Timestamp'         => $date,
                                'TransactionType'   => 'CustomerPayBillOnline',
                                'Amount'            => '100',
                                'PartyA'            => "{$answer->getText()}",
                                'PartyB'            => '684796',
                                'PhoneNumber'       => "{$answer->getText()}",
                                'CallBackURL'       => 'https://compwhiz.co.ke/newincoming',
                                'AccountReference'  => 'Book',
                                'TransactionDesc'   => 'sample'
                            );

                            $data_string = json_encode($curl_post_data);

                            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($curl, CURLOPT_POST, true);
                            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

                            $curl_response = curl_exec($curl);

                            $this->bot->typesAndWaits(4000);
                            $this->say("We are confirming your transaction. You will get the Game via text");

                        });

                    } else {
                        $this->say(Inspiring::quote());
                    }
                }
            }, [
                'parse_mode' => 'Markdown',
            ]);

        }, ['reply_markup' => json_encode([
            'keyboard'          => $g,
            'one_time_keyboard' => true,
            'resize_keyboard'   => true
        ])]);

    }

    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askReason();
    }

    /**
     * @return array
     */
    public function betikagames(): array
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->get('https://api.betika.com/v1/uo/matches?page=1&limit=50&keyword=&tab= &sub_type_id=1,186&tag_id=');
        $d = collect(json_decode($res->getBody(), true))['data'];

        $g = collect($d)->map(function ($foota) {
            return ["{$foota['home_team']} vs {$foota['away_team']} | {$foota['game_id']}",];
        });
        return array($d, $g);
    }
}
