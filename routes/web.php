<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');


Route::get('search', function () {
    $client = new \GuzzleHttp\Client();
    $res = $client->get('https://www.sportpesa.co.ke/api/todays/1/games?type=prematch&section=today&o=startTime&pag_count=30&pag_min=1');
    $d = collect(json_decode($res->getBody(), true));
    $g = collect($d)->where('smsId', 5218)->first();
    return $g;

    return $matches[0][0];
});

Route::get('curl', function () {
    echo "Check if curl is loaded </br>";
    var_dump(extension_loaded('curl'));

    echo "Curl is not activated";
    echo "</br>";
    echo "<pre>";
    print_r(get_loaded_extensions());
    echo "</pre>";
});

Route::get('games', function () {
    $client = new \GuzzleHttp\Client();
    $res = $client->get('https://api.betika.com/v1/uo/matches?page=1&limit=50&keyword=&tab= &sub_type_id=1,186&tag_id=');
    $d = collect(json_decode($res->getBody(), true))['data'];

    return collect($d)->each(function ($games) {
        return $games;
    });
});

Route::post('safaricom', function (\Illuminate\Http\Request $request) {
    \Illuminate\Support\Facades\Log::info('Mpesa', [$request->all(),
    ]);
});


